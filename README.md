after loading your module into the packages dir, add the specific package to the sys.path in your dag to be able to properly access the main.py file:


sys.path.append('/opt/airflow/packages/datamirror/')

To deal with packages without interfering with system python, pip install -r requirements.txt --target=path/to/local/packages/folder
Probably end up installing these into /opt/airflow/pip-additions under custom project names and adding paths in the dags